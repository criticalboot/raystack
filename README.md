# rayStack Platform

![rayStack](raystack/meta/stackex.png)

**N.B. The project is at the planning stage!**

### Instead of Specs

* Dimension 20x20 mm (but 24x24 mm still also under review).
* DF40 30 pin mezzanine connector for stacking.
  Heights available: 1.5 mm, 2.0 mm, 2.5 mm, 3.0 mm, 3.5 mm.
* One SMT spacer for board assembly. Matches the mated height,
  can be metal with thread, or plastic.

### Cards Available

* [thirtytwo](thirtytwo/) - the nRF52832 QFN48 slice with built-in antenna.
  Four layers, mating height 1.5 mm.
* [orient](orient/) - 9-axis IMU low-power solution.
* [2032](2032/) - attaches CR2032, 2025, or 2016 battery to the stack.


Stay tuned!

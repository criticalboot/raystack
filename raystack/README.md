# rayStack Template

![Board Image](meta/board.png)

These is the template for a rayStack board.

Please replace this section with actual description on new board...

## Board Specification

Capability              | Value
------------------------|------------------
Board size              | standard (20x20 mm)
Stack connector         | yes / only front / only bottom
Mating height           | 1.5 mm
PCB substrate           | FR-4
PCB thickness           | 1.6 mm
Layers                  | 2 layers
Finished copper         | 1 oz/sq.ft
Min track / spacing     | 6 mil = 0.16 mm
Min via hole drill size | 12 mil = 0.3 mm
Min via annular ring    | 5 mil = 0.125 mm

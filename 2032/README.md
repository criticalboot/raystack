# The 2032

![Board Image](2032.png)

The board attaches a lithium 3V, 20 mm button cell to the rayStack.
It has mounting through holes for the following battery holders,
by Linx:

* BAT-HLD-001-THM - accepts CR2032, CR2025 batteries
* BAT-HLD-002-THM - accepts low profile CR2016 battery

The board features a protection diode.

## Board Specification

Capability              | Value
------------------------|------------------
Board size              | standard (20x20 mm)
Stack connector         | only front
Mating height           | 1.5 mm
PCB substrate           | FR-4
PCB thickness           | 1.0 mm
Layers                  | 2 layers
Finished copper         | 1 oz/sq.ft
Min track / spacing     | 6 mil = 0.16 mm
Min via hole drill size | 12 mil = 0.3 mm
Min via annular ring    | 6 mil = 0.15 mm

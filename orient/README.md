# The ORIENT

![Board Image](orient.png)

The Orient board provides compact, full featured, precise, 9-axis
absolute orientation MEMS solution.  Thanks to the ultra-low power
components it allows maintain drain current at 5 µA in standby, and
below 2.2 mA at full throttle thus extending button battery life:

* ST LSM303AGR -  high-performance 3D digital linear accelerometer
  and a 3D digital magnetometer
* Bosch BMG250 - low noise, low power three axial gyroscope

## Board Specification

Capability              | Value
------------------------|------------------
Board size              | standard (20x20 mm)
Stack connector         | yes
Mating height           | 1.5 mm
PCB substrate           | FR-4
PCB thickness           | 0.8 mm
Layers                  | 2 layers
Finished copper         | 1 oz/sq.ft
Min track / spacing     | 6 mil = 0.16 mm
Min via hole drill size | 12 mil = 0.3 mm
Min via annular ring    | 5 mil = 0.125 mm

# The thirtytwo

![Board Image](thirtytwo.png)

The thirtytwo offers a complete solution for the Nordic nRF52832
MCU in QFN48 package.

Some features include:

* Integrated monopole antenna
* On-chip DC-DC buck converter
* Low-power 32kHz crystal
* NFC interface
* 10 high-speed GPIO with 4 analog inputs
* On-board status LED
* On-board hardware reset pad
* Serial Wire Debug (SWD)
* Low cost four layers layout

## Board Specification

Capability              | Value
------------------------|------------------
Board size              | standard (20x20 mm)
Stack connector         | yes
Mating height           | 1.5 mm
PCB substrate           | FR-4
PCB thickness           | 1.6 mm
Layers                  | 4 layers
Finished copper         | 1 oz/sq.ft
Min track / spacing     | 6 mil = 0.16 mm
Min via hole drill size | 12 mil = 0.3 mm
Min via annular ring    | 5 mil = 0.125 mm
